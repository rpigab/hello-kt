# hello-kt

Kotlin + Spring Boot + hexagonal Hello World app.

## TODO

- Fix mapstruct codegen plugin
- Add cucumber e2e tests

## Resources

- https://www.baeldung.com/maven-multi-module
- https://medium.com/wearewaes/how-to-run-end-to-end-tests-in-a-maven-multi-module-project-f3675e260a55
