package pigab.hellokt

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.openfeign.EnableFeignClients

@SpringBootApplication
@EnableFeignClients
class HelloKtApplication

fun main(args: Array<String>) {
    runApplication<HelloKtApplication>(*args)
}
