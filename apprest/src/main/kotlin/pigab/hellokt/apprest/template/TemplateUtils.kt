package pigab.hellokt.apprest.template

import kotlinx.html.*

class TemplateUtils {

    companion object {
        fun HEAD.favicon() {
            link {
                rel = "icon"
                href = "data:,"
            }
        }

        fun HEAD.htmxScript() {
            script {
                src = "https://unpkg.com/htmx.org@1.9.10"
                integrity = "sha384-D1Kt99CQMDuVetoL1lrYwg5t+9QdHe7NLX/SoJYkXDFfX37iInKRy5xLSi8nO7UC"
                attributes["crossorigin"] = "anonymous"
            }
        }

        fun DIV.li(e: LI.() -> Unit) = LI(
            initialAttributes = mutableMapOf(), consumer = consumer
        ).visit(e)
    }
}
