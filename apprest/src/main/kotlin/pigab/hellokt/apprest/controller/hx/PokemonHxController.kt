package pigab.hellokt.apprest.controller.hx

import kotlinx.html.*
import kotlinx.html.dom.createHTMLDocument
import kotlinx.html.dom.serialize
import org.springframework.http.MediaType
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import pigab.hellokt.apprest.template.TemplateUtils.Companion.favicon
import pigab.hellokt.apprest.template.TemplateUtils.Companion.htmxScript
import pigab.hellokt.domain.service.PokemonService

@Controller
@RequestMapping("hx/pokemon", produces = [MediaType.TEXT_HTML_VALUE])
class PokemonHxController(
    val pokemonService: PokemonService,
) {

    @GetMapping("list")
    @ResponseBody
    fun main() = createHTMLDocument().html {
        head {
            title { +"Pokémon" }
            htmxScript()
            favicon()
        }
        body {
            div {
                h1 { +"Pokémon Search" }
                div {
                    htmxSearchInput()
                    div {
                        id = "search-results"
                    }
                }
            }
        }
    }.serialize()

    fun FlowContent.htmxSearchInput() {
        input {
            classes = setOf("form-control")
            attributes["type"] = "search"
            attributes["name"] = "search"
            attributes["placeholder"] = "Begin Typing To Search Pokemons..."
            attributes["hx-post"] = "search"
            attributes["hx-trigger"] = "input changed delay:500ms, search"
            attributes["hx-target"] = "#search-results"
        }
    }

    @PostMapping("search")
    @ResponseBody
    fun search(@ModelAttribute("search") search: String) = createHTMLDocument().ul {
        for (p in pokemonService.search(search)) {
            li {
                +"#${p.id} - ${p.name}"
            }
        }
    }.serialize()

}
