package pigab.hellokt.apprest.controller.rest

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("rest/hello")
class HelloRestController {

    @GetMapping
    fun hello() = "hello world"

}
