package pigab.hellokt.apprest.controller.hx

import kotlinx.html.*
import kotlinx.html.FormMethod.post
import kotlinx.html.InputType.*
import kotlinx.html.dom.createHTMLDocument
import kotlinx.html.dom.serialize
import mu.KotlinLogging
import org.springframework.http.MediaType
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.ResponseBody
import pigab.hellokt.apprest.template.TemplateUtils.Companion.favicon

private val log = KotlinLogging.logger {}

@Controller
@RequestMapping("hx/hello", produces = [MediaType.TEXT_HTML_VALUE])
class HelloHxController {

    companion object {
        const val HOME = "home"
        const val HELLO = "hello"
        const val LOGIN = "login"
        const val LOGOUT = "logout"
    }

    @GetMapping(HOME)
    @ResponseBody
    fun home() = createHTMLDocument().html {
        head {
            title { +"Hello - home" }
            favicon()
        }
        body {
            p {
                +"Welcome!"
                br
                a {
                    href = HELLO
                    +"Click here for a greeting: $HELLO"
                }
            }
        }
    }.serialize()

    @GetMapping(HELLO)
    @ResponseBody
    fun hello() = createHTMLDocument().html {
        head {
            title { +"Hello - hello" }
            favicon()
        }
        body {
            p {
                +"Hello ##Name##!"
            }
            form {
                action = LOGOUT
                method = post
                input {
                    type = submit
                    value = "Sign Out"
                }
            }
        }
    }.serialize()

    @GetMapping(LOGIN)
    @ResponseBody
    fun login(
        @RequestParam("error") error: String?,
        @RequestParam("logout") logout: String?,
    ): String {
        return createHTMLDocument().html {
            head {
                title { +"Hello - login" }
                favicon()
            }
            body {
                if (error != null) {
                    div {
                        +"Invalid username and password."
                    }
                }
                if (logout != null) {
                    div {
                        +"You have been logged out."
                    }
                }
                form {
                    action = LOGIN
                    method = post
                    div {
                        label {
                            +"User name: "
                            input {
                                type = text
                                name = "username"
                            }
                        }
                    }
                    div {
                        label {
                            +"Password: "
                            input {
                                type = password
                                name = "password"
                            }
                        }
                    }
                    div {
                        input {
                            type = submit
                            value = "Sign In"
                        }

                    }
                }
            }
        }.serialize()
    }

}
