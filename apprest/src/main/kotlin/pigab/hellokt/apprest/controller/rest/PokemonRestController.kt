package pigab.hellokt.apprest.controller.rest

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.ModelAttribute
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import pigab.hellokt.apprest.client.PokedexClient
import pigab.hellokt.apprest.dto.PokemonDto
import pigab.hellokt.apprest.dto.PokemonSimpleDto
import pigab.hellokt.apprest.mapper.PokemonDtoMapper
import pigab.hellokt.apprest.mapper.PokemonSimpleDtoMapper
import pigab.hellokt.domain.service.PokemonService

@RestController
@RequestMapping("rest/pokemon")
class PokemonRestController(
    val pokemonService: PokemonService,
    val pokemonSimpleDtoMapper: PokemonSimpleDtoMapper,
    val pokemonDtoMapper: PokemonDtoMapper,
    val pokedexClient: PokedexClient,
) {

    @GetMapping("{id}")
    fun getOne(@PathVariable id: Long): PokemonSimpleDto? =
        pokemonService.findById(id)
            .map { pokemonSimpleDtoMapper.toDto(it) }.orElseThrow()

    @GetMapping
    fun getAll(): List<PokemonSimpleDto> = pokemonService.findAll()
        .map { pokemonSimpleDtoMapper.toDto(it) }.toList()

    @GetMapping("page")
    fun getPage(): List<PokemonSimpleDto> = pokemonService.findPage()
        .map { pokemonSimpleDtoMapper.toDto(it) }.toList()

    @PostMapping("search")
    fun search(@ModelAttribute("search") search: String) = pokemonService.search(search)
        .map { pokemonSimpleDtoMapper.toDto(it) }.toList()

    @PostMapping
    fun create(@RequestBody pokemonDto: PokemonSimpleDto) =
        pokemonService.save(pokemonSimpleDtoMapper.toBean(pokemonDto))

    @PostMapping("import")
    fun import(@RequestBody pokemonDtoList: List<PokemonDto>) {
        val pokemonBeanList = pokemonDtoList.toList()
            .map { pokemonDtoMapper.toBean(it) }.toList()

        pokemonService.saveAll(pokemonBeanList)
    }

    @PostMapping("import-from-github")
    fun importFromGithub() {
        val pokemonBeanList = pokedexClient.getPokedex()
            .map { pokemonDtoMapper.toBean(it) }.toList()

        pokemonService.saveAll(pokemonBeanList)
    }

}
