package pigab.hellokt.apprest.client

import org.springframework.cloud.openfeign.FeignClient
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import pigab.hellokt.apprest.dto.PokemonDto

@FeignClient("pokedex")
interface PokedexClient {

    @RequestMapping(method = [RequestMethod.GET], value = ["/pokedex.json"])
    fun foobar(): String

    @RequestMapping(method = [RequestMethod.GET], value = ["/pokedex.json"])
    fun getPokedex(): List<PokemonDto>

}
