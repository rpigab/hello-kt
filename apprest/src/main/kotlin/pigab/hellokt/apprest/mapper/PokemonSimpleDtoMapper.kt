package pigab.hellokt.apprest.mapper

import org.mapstruct.Mapper
import pigab.hellokt.apprest.dto.PokemonSimpleDto
import pigab.hellokt.domain.bean.PokemonBean

@Mapper
interface PokemonSimpleDtoMapper {
    fun toDto(pokemonBean: PokemonBean): PokemonSimpleDto

    fun toBean(pokemonDto: PokemonSimpleDto): PokemonBean
}
