package pigab.hellokt.apprest.mapper

import org.mapstruct.Mapper
import org.mapstruct.Mapping
import pigab.hellokt.apprest.dto.PokemonDto
import pigab.hellokt.domain.bean.PokemonBean

@Mapper
interface PokemonDtoMapper {

    @Mapping(source = "name.english", target = "name")
    fun toBean(pokemonDto: PokemonDto): PokemonBean
}
