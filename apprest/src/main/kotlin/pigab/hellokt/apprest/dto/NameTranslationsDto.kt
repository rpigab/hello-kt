package pigab.hellokt.apprest.dto

class NameTranslationsDto(
    var english: String,
    var japanese: String?,
    var chinese: String?,
    var french: String?,
)
