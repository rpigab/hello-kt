package pigab.hellokt.apprest.dto

class PokemonDto(
    var id: Long,
    var name: NameTranslationsDto,
)
