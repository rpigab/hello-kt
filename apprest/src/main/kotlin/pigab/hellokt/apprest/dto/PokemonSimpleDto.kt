package pigab.hellokt.apprest.dto

class PokemonSimpleDto(
    var id: Long,
    var name: String,
)
