package pigab.hellokt.apprest.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.MediaType
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter
import java.nio.charset.StandardCharsets


@Configuration
class ClientConfiguration {

    /**
     * Accept to find JSON here
     * even though GitHub's raw endpoints can serve anything wrapped in a text/plain response.
     */
    @Bean
    fun mappingJackson2HttpMessageConverter(): MappingJackson2HttpMessageConverter {
        val converter = MappingJackson2HttpMessageConverter()

        val textPlainUtf = MediaType("text", "plain", StandardCharsets.UTF_8)
        val jacksonTypes: List<MediaType> = ArrayList(converter.supportedMediaTypes)
        jacksonTypes.addLast(textPlainUtf)
        converter.supportedMediaTypes = jacksonTypes
        return converter
    }
}
