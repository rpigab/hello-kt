package pigab.hellokt.domain.bean

data class PokemonBean(
    var id: Long,
    var name: String,
)
