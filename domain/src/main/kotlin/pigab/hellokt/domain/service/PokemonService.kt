package pigab.hellokt.domain.service

import org.springframework.stereotype.Service
import pigab.hellokt.domain.bean.PokemonBean
import pigab.hellokt.domain.port.PokemonPort
import java.util.Optional

@Service
class PokemonService(
    private val pokemonPort: PokemonPort
) {

    fun findAll(): List<PokemonBean> = pokemonPort.findAll()

    fun search(search: String): List<PokemonBean> = pokemonPort.search(search)

    fun findPage() = pokemonPort.getPage()

    fun findById(id: Long): Optional<PokemonBean> = pokemonPort.findById(id)

    fun save(pokemonToSave: PokemonBean): Any = pokemonPort.save(pokemonToSave)

    fun saveAll(pokemonToSave: Iterable<PokemonBean>) = pokemonPort.saveAll(pokemonToSave)

}
