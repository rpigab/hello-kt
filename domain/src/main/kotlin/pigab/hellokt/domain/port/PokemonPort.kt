package pigab.hellokt.domain.port

import org.springframework.data.domain.Slice
import pigab.hellokt.domain.bean.PokemonBean
import java.util.Optional

interface PokemonPort {
    fun findAll(): List<PokemonBean>
    fun search(search: String): List<PokemonBean>
    fun getPage(): Slice<PokemonBean>
    fun findById(id: Long): Optional<PokemonBean>
    fun save(pokemonToSave: PokemonBean): Any
    fun saveAll(pokemonsToSave: Iterable<PokemonBean>): Any
}
