package pigab.hellokt.domain.service

import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.junit.jupiter.MockitoExtension
import pigab.hellokt.domain.bean.PokemonBean
import pigab.hellokt.domain.port.PokemonPort

@ExtendWith(MockitoExtension::class)
class PokemonServiceTest {

    @InjectMocks
    private lateinit var pokemonService: PokemonService

    @Mock
    private lateinit var pokemonPort: PokemonPort

    @Test
    // TODO re-enable
    @Disabled
    fun findAll() {
        val pokemons = listOf(PokemonBean(1L, "Charmander"))

        `when`(pokemonPort.findAll()).thenReturn(pokemons)

        val res = pokemonService.findAll()

        assert(res == pokemons)
    }

}
