package pigab.hellokt.repository

import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.stereotype.Component
import org.springframework.stereotype.Repository
import pigab.hellokt.entity.PokemonEntity

@Repository
@Component
interface PokemonRepository :
    PagingAndSortingRepository<PokemonEntity, Long>,
    CrudRepository<PokemonEntity, Long> {

    fun findByNameContainingIgnoreCase(pattern: String): List<PokemonEntity>
}
