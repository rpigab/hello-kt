package pigab.hellokt.adapter

import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Slice
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional
import pigab.hellokt.domain.bean.PokemonBean
import pigab.hellokt.domain.port.PokemonPort
import pigab.hellokt.entity.mapper.PokemonEntityMapper
import pigab.hellokt.repository.PokemonRepository
import java.util.Optional

@Component
class PokemonAdapter(
    private val pokemonRepository: PokemonRepository,
    private val pokemonEntityMapper: PokemonEntityMapper,
) : PokemonPort {

    @Transactional(readOnly = true)
    override fun findAll(): List<PokemonBean> =
        pokemonRepository.findAll().map { pokemonEntityMapper.toBean(it) }.toList()

    @Transactional(readOnly = true)
    override fun search(search: String): List<PokemonBean> =
        pokemonRepository.findByNameContainingIgnoreCase(search)
            .map { pokemonEntityMapper.toBean(it) }

    @Transactional(readOnly = true)
    override fun getPage(): Slice<PokemonBean> {
        return pokemonRepository.findAll(PageRequest.of(0, 5))
            .map { pokemonEntityMapper.toBean(it) }
    }

    @Transactional(readOnly = true)
    override fun findById(id: Long): Optional<PokemonBean> =
        pokemonRepository.findById(id).map { pokemonEntityMapper.toBean(it) }

    @Transactional
    override fun save(pokemonToSave: PokemonBean) = pokemonRepository.save(pokemonEntityMapper.toEntity(pokemonToSave))

    @Transactional
    override fun saveAll(pokemonsToSave: Iterable<PokemonBean>): List<PokemonBean> {

        val pokemonEntitiesToSave = pokemonsToSave.map { pokemonEntityMapper.toEntity(it) }
        return pokemonRepository.saveAll(pokemonEntitiesToSave).map { pokemonEntityMapper.toBean(it) }
    }
}
