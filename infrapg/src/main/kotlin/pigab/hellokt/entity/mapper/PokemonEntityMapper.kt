package pigab.hellokt.entity.mapper

import org.mapstruct.Mapper
import pigab.hellokt.domain.bean.PokemonBean
import pigab.hellokt.entity.PokemonEntity

@Mapper
interface PokemonEntityMapper {
    fun toEntity(pokemonBean: PokemonBean): PokemonEntity

    fun toBean(pokemonEntity: PokemonEntity): PokemonBean
}
