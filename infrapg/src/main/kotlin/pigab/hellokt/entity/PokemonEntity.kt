package pigab.hellokt.entity

import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.Id
import jakarta.persistence.Table

@Entity
@Table(name = "pokemon")
class PokemonEntity(
    @Id @Column var id: Long,
    @Column var name: String,
)
